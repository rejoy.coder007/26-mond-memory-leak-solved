package com.mond.aa_HomeScreen.ab_FavoritesTab;

import android.os.Parcel;
import android.os.Parcelable;

public class Paramter_Parcelable implements Parcelable {
    private String mText1;
    private String mText2;

    public Paramter_Parcelable(  String text1, String text2) {

        mText1 = text1;
        mText2 = text2;
    }

    protected Paramter_Parcelable(Parcel in) {

        mText1 = in.readString();
        mText2 = in.readString();
    }

    public static final Creator<Paramter_Parcelable> CREATOR = new Creator<Paramter_Parcelable>() {
        @Override
        public Paramter_Parcelable createFromParcel(Parcel in) {
            return new Paramter_Parcelable(in);
        }

        @Override
        public Paramter_Parcelable[] newArray(int size) {
            return new Paramter_Parcelable[size];
        }
    };



    public String getText1() {
        return mText1;
    }

    public String getText2() {
        return mText2;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {

        dest.writeString(mText1);
        dest.writeString(mText2);
    }
}