package com.mond.aa_HomeScreen.ab_FavoritesTab.aa_Recycler;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.geometry.mond.mond.R;

public class ViewHolder extends RecyclerView.ViewHolder {


    public TextView dir_name;
    public TextView video_count;
    public LinearLayout slice;


    public ViewHolder(@NonNull View itemView) {
        super(itemView);

        dir_name = itemView.findViewById(R.id.video_thumb);
        video_count = itemView.findViewById(R.id.file_name);
        slice  = itemView.findViewById(R.id.slice);
    }
}
