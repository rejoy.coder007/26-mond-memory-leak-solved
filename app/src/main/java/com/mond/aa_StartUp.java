package com.mond;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import com.mond.aa_HomeScreen.home_screen;
import com.geometry.mond.mond.R;
import com.mond.dummy.Main2Activity;
import com.mond.dummy.MainActivity;
import com.mond.za_global.MyApplication;

import java.io.File;
import java.lang.ref.WeakReference;


public class aa_StartUp extends AppCompatActivity {

    private aa_StartUp aa_startUp;

    private BackgroundTask backgroundTask;

    private static class MyHandler extends Handler {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            Log.e("LOG", "Hello~1");
        }
    }


    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        ((MyApplication) getApplication()).map.clear();
        ((MyApplication) getApplication()).names.clear();
        // mActivity = new WeakReference<aa_StartUp>(aa_StartUp.this);



        if (isFirstTime()) {

            setContentView(R.layout.aa_a_activity_start_up);

            Handler handler1 = new MyHandler();

            handler1.postDelayed(new Runnable() {
                @Override
                public void run() {

                    startActivity(new Intent(aa_StartUp.this, MainActivity.class));
                    //  finish();
                }
            }, 3000);


        }

        else
        {

            setContentView(R.layout.aa_b_activity_splash_up);
            Handler handler1 = new MyHandler();

            handler1.postDelayed(new Runnable() {
                @Override
                public void run() {

                    startActivity(new Intent(aa_StartUp.this, Main2Activity.class));
                    //  finish();
                }
            }, 3000);

        }



        //  backgroundTask = new BackgroundTask(aa_StartUp.this);
       // backgroundTask.execute();






       // finish();

    }


    private boolean isFirstTime() {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);

        boolean ranBefore = preferences.getBoolean("Permission", false);
        Log.d("FIRST_START", "isFirstTime: " + ranBefore);
        return !ranBefore;
    }



    private static class BackgroundTask extends AsyncTask<Void, Void, String> {


        private final WeakReference<aa_StartUp> messageViewReference;

        private BackgroundTask(aa_StartUp aa_startUp) {
            this.messageViewReference = new WeakReference<>(aa_startUp);
        }

        @Override
        protected String doInBackground(Void... voids)
        {

            try
            {
                Thread.sleep(1000);
            }
            catch (InterruptedException e)
            {
                e.printStackTrace();
            }
            return "The task is completed!";
        }

        @Override
        protected void onPostExecute(String s)
        {
            super.onPostExecute(s);

            aa_StartUp aa_startUp = messageViewReference.get();
            Intent intent = new Intent(aa_startUp, home_screen.class);

            //   intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);

            aa_startUp.startActivity(intent);

           // aa_startUp.finish();

        }
    }

    @Override
    protected void onDestroy()
    {
        super.onDestroy();

        /*
         * Fix number 2
         * */
        if(backgroundTask != null)
        {
            backgroundTask.cancel(true);
        }
    }


}


