package com.mond.ab_FolderScreen.ab_FolderTab;

import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.geometry.mond.mond.R;
import com.mond.aa_StartUp;
import com.mond.ab_FolderScreen.ab_FolderTab.aa_Recycler.Directory;
import com.mond.ab_FolderScreen.ab_FolderTab.aa_Recycler.RecyclerAdapter;
import com.mond.ab_FolderScreen.folder_screen;
import com.mond.za_global.MyApplication;

import java.util.ArrayList;
import java.util.List;

public class FavoritesFolderFragment extends Fragment  {
    SwipeRefreshLayout mSwipeRefreshLayout;

    View rootView;
    RecyclerView recyclerView;
    Boolean mStopLoop = false;
    RecyclerAdapter recyclerAdapter;
    Handler handler;
    List<Directory> directories = new ArrayList<>();
    public String dir;

    /*
    List<fav_dir> listCont;

*/
    public FavoritesFolderFragment() {

        //

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {

        handler = new Handler(getContext().getMainLooper());
        rootView = inflater.inflate(R.layout.ab_b_frag_favorites_layout, container, false);
        recyclerView = (RecyclerView) rootView.findViewById(R.id.contact_recycleView);

        final LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(linearLayoutManager);

        recyclerView.setHasFixedSize(true);

        recyclerAdapter=new RecyclerAdapter(directories, getContext());
        recyclerView.setAdapter(recyclerAdapter);


        // SwipeRefreshLayout
        mSwipeRefreshLayout = (SwipeRefreshLayout) rootView.findViewById(R.id.swipe_container);
        mSwipeRefreshLayout.setColorSchemeResources(R.color.colorAccent,
                android.R.color.holo_green_dark,
                android.R.color.holo_orange_dark,
                android.R.color.holo_blue_dark);

     //   StartThread_files(dir);







        return rootView;
    }

    public void add_refresh_listener(final String dir){
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                // Your recyclerview reload logic function will be here!!!
                //     Log.d("#REFRESH", "onRefresh: ");

                StartThread_files(dir);

                //mSwipeRefreshLayout.setRefreshing(false);

            }
        });
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


    }


    public void StartThread_files(final String file)
    {
        //Toast.makeText(getApplicationContext(), "Your Message", Toast.LENGTH_LONG).show();


        new Thread(new Runnable()
        {
            public void run()
            {


                ((MyApplication) (getActivity()). getApplication()).getFilesFromDir(file);
                directories.clear();

                for (int i = 0; i < ((MyApplication) (getActivity()).getApplication()).names.size(); i++)
                {

                    List<String> values = new ArrayList<String>();
                    values = ((MyApplication) (getActivity()).getApplication()).map.get(((MyApplication) (getActivity()).getApplication()).names.get(i));
            //        Log.d("_DIR_N", "getfile: " + ((MyApplication) (getActivity()).getApplication()).names.get(i));


                    Directory directory = new Directory(((MyApplication) (getActivity()).getApplication()).names.get(i), values.get(0),values.get(1));

                    directories.add(directory);
                }



                try
                {
                    Thread.sleep(0);

                    handler.post(new Runnable()
                    {
                        @Override
                        public void run()
                        {
                            // recyclerAdapter.notifyDataSetChanged();
                            recyclerAdapter.setRecyclerAdapter(directories,getContext());
                            // recyclerAdapter.notifyItemRangeChanged(0, directories.size());
                            recyclerAdapter.notifyDataSetChanged();
                            mSwipeRefreshLayout.setRefreshing(false);

                            Log.d("#_DIR_TEST", "names: " + ((MyApplication) (getActivity()).getApplication()).names);
                            Log.d("#_DIR_TEST", "map: " + ((MyApplication) (getActivity()).getApplication()).map);


                            //       enable_tab();
                        }
                    });


                }
                catch (InterruptedException e)
                {
                    e.printStackTrace();
                }
                Log.i("#INSIDE", " thread id: " + Thread.currentThread().getId());


            }
        }).start();


    }




}
