package com.mond.ab_FolderScreen.ab_FolderTab.aa_Recycler;

public class Directory {

    public Directory(String video_thumb, String file_name, String path_file) {
        this.video_thumb = video_thumb;
        this.file_name = file_name;
        this.path_file = path_file;
    }

    public String video_thumb;


    public String getVideo_thumb() {
        return video_thumb;
    }

    public void setVideo_thumb(String video_thumb) {
        this.video_thumb = video_thumb;
    }

    public String getFile_name() {
        return file_name;
    }

    public void setFile_name(String file_name) {
        this.file_name = file_name;
    }

    public String getPath_file() {
        return path_file;
    }

    public void setPath_file(String path_file) {
        this.path_file = path_file;
    }

    public String file_name;

    public String path_file;
}
