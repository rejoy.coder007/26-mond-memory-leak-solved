package com.mond.ab_FolderScreen.ab_FolderTab.aa_Recycler;

import android.content.ContentResolver;
import android.content.Context;
import android.graphics.Color;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.geometry.mond.mond.R;
import com.mond.zc_Glide.FutureStudioAppGlideModule;

import java.io.File;
import java.util.List;

public class RecyclerAdapter extends RecyclerView.Adapter<ViewHolder> {
    FutureStudioAppGlideModule futureStudioAppGlideModule;
    private List<Directory> directories;
    private Context context;

    public RecyclerAdapter(List<Directory> directories, Context context) {
        this.directories = directories;
        this.context = context;
        this.futureStudioAppGlideModule = new FutureStudioAppGlideModule();
    }

    public void setRecyclerAdapter(List<Directory> directories, Context context) {
        this.directories = directories;
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {


        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.ae_slice_frag_fav_folder_recycler_item, viewGroup, false);


        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, final int i) {


        Directory directory = this.directories.get(i);
       // viewHolder.video_thumb.setText(directory.video_thumb);

        String string_path = directory.file_name;
        String[] parts = string_path.split(".");
       // viewHolder.file_name.setText(parts[parts.length-1]);

        viewHolder.file_name.setText( string_path.substring(0, string_path.lastIndexOf(".")));

        long interval = 1 * 200;
        //RequestOptions options = new RequestOptions().frame(interval);

        RequestOptions options = new RequestOptions();
        options.fitCenter();

       // int width = 200;
       // int height = 200;
       // LinearLayout.LayoutParams parms = new LinearLayout.LayoutParams(width,height);
      //  viewHolder.video_thumb.setLayoutParams(parms);



        futureStudioAppGlideModule.add_image_to_view(context,viewHolder,Uri.fromFile( new File( directory.path_file ) ));





        //viewHolder.video_thumb.setBackgroundResource(R.drawable.ab_crown);

        // options.frame(interval);

        /*
        Glide.with(context).asBitmap()
                .load(Uri.fromFile( new File( directory.path_file ) ))
                .apply(options)

                .into(viewHolder.video_thumb);
                */

       // Glide.with(context).load(Uri.fromFile( new File( directory.path_file ) )) .apply(new RequestOptions().centerCrop()).into( viewHolder.video_thumb );

/*
        GlideApp
                .with(context)
                .load(UsageExampleListViewAdapter.eatFoodyImages[0])
                .override(600, 200) // resizes the image to these dimensions (in pixel)
                .centerCrop() // this cropping technique scales the image so that it fills the requested bounds and then crops the extra.
                .into(imageViewResizeCenterCrop);

*/
    //    Glide
      //          .with( context )
      //          .load( Uri.fromFile( new File( directory.path_file ) ) )
      //          .into( viewHolder.video_thumb );



/*
        if(i==0)
        {
            viewHolder.video_thumb.setTextColor(Color.parseColor("#006400"));
        }

        viewHolder.file_name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(context, "Item " + (i + 1) + " clicked",
                        Toast.LENGTH_SHORT).show();
            }
        });

        */

       // int width = viewHolder.video_thumb.getWidth();
       // int height = viewHolder.video_thumb.getHeight();
        //Log.d("#WIDTH_ITEM", "Width : height  "+width+"::"+height);
    }

    @Override
    public int getItemCount() {
        return directories.size();
    }
}
