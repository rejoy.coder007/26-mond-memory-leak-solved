package com.mond.ab_FolderScreen.ab_FolderTab.aa_Recycler;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.geometry.mond.mond.R;

public class ViewHolder extends RecyclerView.ViewHolder {


    public TextView file_name;

    public ImageView video_thumb;


    public ViewHolder(@NonNull View itemView) {
        super(itemView);

       // video_thumb = itemView.findViewById(R.id.video_thumb);
         file_name = itemView.findViewById(R.id.file_name);
        video_thumb  = itemView.findViewById(R.id.video_thumb);


    }
}
