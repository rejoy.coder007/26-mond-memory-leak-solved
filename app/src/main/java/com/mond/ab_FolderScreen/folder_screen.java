package com.mond.ab_FolderScreen;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.FileObserver;
import android.os.Handler;
import android.support.design.widget.TabLayout;
import android.support.v4.view.MenuCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.widget.Toast;

import com.geometry.mond.mond.R;
import com.mond.aa_HomeScreen.ab_FavoritesTab.Paramter_Parcelable;
import com.mond.ab_FolderScreen.ab_FolderTab.FavoritesFolderFragment;
import com.mond.zb_shared_screen.aa_Tabs.aa_SpaceShareTab.SpaceShareFragment;
import com.mond.zb_shared_screen.ab_Pager.ViewPagerAdapter;

public class folder_screen extends AppCompatActivity {
    private int MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE = 20;
    public Toolbar toolbar;
    TabLayout tabLayout;
    ViewPager viewPager;
    Handler handler;

    Context context;

    FavoritesFolderFragment fragment_fav;

    Animation animation;


    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ab_a_activity_home_screen);


        Intent intent = getIntent();
        Paramter_Parcelable paramter_parcelable = intent.getParcelableExtra("DirectoryPath");


        String dir = paramter_parcelable.getText1();


        fragment_fav = new FavoritesFolderFragment();

        fragment_fav.dir=dir;

        Window window = getWindow();
        Drawable background = getResources().getDrawable(R.drawable.ab_toolbar_bg);
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.setStatusBarColor(getResources().getColor(android.R.color.transparent));
        window.setNavigationBarColor(getResources().getColor(android.R.color.transparent));
        window.setBackgroundDrawable(background);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
      //  toolbar.setNavigationOnClickListener(arrow -> onBackPressed());

        handler = new Handler(getApplicationContext().getMainLooper());


        enable_tab();



    }




    @Override
    public boolean onCreateOptionsMenu(Menu menu) {


        MenuCompat.setGroupDividerEnabled(menu, true);


        MenuInflater infl = getMenuInflater();
        infl.inflate(R.menu.main_menu, menu);


        return true;

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        if (id == R.id.item1_id) {

            Toast.makeText(getApplicationContext(), "item1 is selected", Toast.LENGTH_SHORT).show();

        } else if (id == R.id.item2_id) {

            Toast.makeText(getApplicationContext(), "item2 is selected", Toast.LENGTH_SHORT).show();


        } else if (id == R.id.item3_id) {

            Toast.makeText(getApplicationContext(), "item3 is selected", Toast.LENGTH_SHORT).show();
        }
        return super.onOptionsItemSelected(item);

    }


    public void enable_tab() {

        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new SpaceShareFragment(), "SpaceShare");
        adapter.addFragment(fragment_fav, "Folder");

        viewPager = findViewById(R.id.viewpager);
        viewPager.setAdapter(adapter);

        tabLayout = findViewById(R.id.tab_layout);
        tabLayout.setupWithViewPager(viewPager);
        viewPager.setCurrentItem(1);

    }









}