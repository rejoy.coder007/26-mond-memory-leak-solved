package com.mond.dummy;

import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.geometry.mond.mond.R;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);

        // first time
        SharedPreferences.Editor editor = preferences.edit();
        editor.putBoolean("Permission", true);
        editor.commit();
        boolean ranBefore = preferences.getBoolean("Permission", false);
    }
}
