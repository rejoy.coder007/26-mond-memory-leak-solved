package com.mond.zb_shared_screen.aa_Tabs.aa_SpaceShareTab;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.geometry.mond.mond.R;

public class SpaceShareFragment extends Fragment {

    public SpaceShareFragment() {

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {


        return inflater.inflate(R.layout.ab_c_frag_shared_screen_layout, container, false);
    }
}
